import { createClient } from '@supabase/supabase-js';

const sbClient = createClient(
  'https://abopgbdzggenzxdakwdo.supabase.co',
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImFib3BnYmR6Z2dlbnp4ZGFrd2RvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzY5MjEzNjAsImV4cCI6MTk5MjQ5NzM2MH0.bXAYzMeQzZf9NwNZUD26uGPsZ6OJqiZ_wqawe3Z2aHQ'
);

export const getServerAuthSession = async ({
  req,
  res,
}: {
  req: any;
  res: any;
}) => {
  const jwt = (req.headers?.authorization?.replace('Bearer ', '') ?? null) as string | null;
  if(!jwt) return null;
  const data= await sbClient.auth.getUser(jwt);
  return data?.data.user ?? null;
};
