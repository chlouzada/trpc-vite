import * as functions from 'firebase-functions';

import express from 'express';
import cors from 'cors';
import * as trpcExpress from '@trpc/server/adapters/express';
import { appRouter, createTRPCContext } from './trpc';

import { config } from 'dotenv';
config();

export const trpc = functions.https.onRequest((request, response) => {
  response.set('Access-Control-Allow-Origin', '*');

  // SETUP ROUTER
  const app = express();
  app.use(cors());
  app.use(
    '/',
    trpcExpress.createExpressMiddleware({
      createContext: createTRPCContext,
      router: appRouter,
    })
  );

  // HANDLE REQUEST
  app(request, response);
});

// EXPORT TYPES OF APP ROUTER
export type AppRouter = typeof appRouter;
