import { authRouter } from './router/auth';
import { userRouter } from './router/user';
import { createTRPCRouter } from './trpc';

// EXPORTS =================================
export { appRouter };
export { createTRPCContext } from './trpc';
export type AppRouter = typeof appRouter;
// =========================================

const appRouter = createTRPCRouter({
  user: userRouter,
  auth: authRouter,
});