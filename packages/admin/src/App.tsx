import { useState } from 'react';
import { LoginPage } from './pages/login';
import { trpc } from './utils/trpc';

function App() {
  const userQuery = trpc.auth.getSecretMessage.useQuery();
  return (
    <div>
      <LoginPage />

      <div>Secret Message: {userQuery.data}</div>

      <div>Erro: {userQuery.error?.message}</div>
    </div>
  );
}

export default App;
