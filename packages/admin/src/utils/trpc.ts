// utils/trpc.ts
import { AppRouter } from 'functions/src/trpc';
import { createTRPCReact } from '@trpc/react-query';

export const trpc = createTRPCReact<AppRouter>();