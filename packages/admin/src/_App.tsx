import React from 'react';
import { useState } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { httpBatchLink } from '@trpc/client';
import App from './App';
import { trpc } from './utils/trpc';
import {
  SessionContextProvider,
  useSession,
} from '@supabase/auth-helpers-react';
import { createClient } from '@supabase/supabase-js';

const sbClient = createClient(
  import.meta.env.VITE_SUPABASE_URL!,
  import.meta.env.VITE_SUPABASE_ANON_KEY!
);
export { sbClient as supabaseClient };

const WithSupabase = ({ children }: { children: React.ReactNode }) => {
  const [supabaseClient] = useState(sbClient);

  return (
    <SessionContextProvider supabaseClient={supabaseClient}>
      {children}
    </SessionContextProvider>
  );
};

const WithTRPC = ({ children }: { children: React.ReactNode }) => {
  const [queryClient] = useState(() => new QueryClient());
  const [trpcClient] = useState(() =>
    trpc.createClient({
      links: [
        httpBatchLink({
          url: import.meta.env.VITE_TRPC_URL!,
          async headers() {
            const jwt = (await sbClient.auth.getSession()).data?.session
              ?.access_token;
            return {
              Authorization: `Bearer ${jwt}`,
            };
          },
        }),
      ],
    })
  );

  return (
    <trpc.Provider client={trpcClient} queryClient={queryClient}>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </trpc.Provider>
  );
};

export default () => {
  return (
    <WithSupabase>
      <WithTRPC>
        <App />
      </WithTRPC>
    </WithSupabase>
  );
};
