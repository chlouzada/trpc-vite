import { type NextPage } from 'next';
import Head from 'next/head';
import { useState } from 'react';
import { supabaseClient } from '../_App';
import { useUser, useSession } from '@supabase/auth-helpers-react';

import * as React from 'react';
import { useMutation } from '@tanstack/react-query';

const CheckEmail = () => {
  return (
    <div>
      <h1>Check your email</h1>
      <p>We sent you a magic link. Click it to sign in.</p>
    </div>
  );
};

export const LoginPage = () => {
  const user = useUser();
  const session = useSession();

  const [email, setEmail] = useState('');

  console.log(window.location.href);
  const redirectTo = window.location.href;

  const opt = useMutation(async ({ email }: { email: string }) => {
    const { error } = await supabaseClient.auth.signInWithOtp({
      email,
      options: {
        emailRedirectTo: redirectTo,
      },
    });
    if (error) throw error;
  });

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    opt.mutate({ email });
  };

  const logout = async () => {
    await supabaseClient.auth.signOut();
  };

  if (user) {
    return (
      <div>
        Logado <button onClick={logout}>Sair</button>
        <p>user: {JSON.stringify(user)}</p>
        <p>session: {JSON.stringify(session)}</p>
      </div>
    );
  }

  return (
    <div>
      {opt.isSuccess ? (
        <CheckEmail />
      ) : (
        <form onSubmit={handleSubmit}>
          <input
            value={email}
            onChange={(event) => setEmail(event.currentTarget.value)}
          />
          <button>Enviar</button>
        </form>
      )}
    </div>
  );
};
