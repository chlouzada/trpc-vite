import React from "react";
import ReactDOM from "react-dom/client";
import _App from "./_App";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <_App />
  </React.StrictMode>
);
